from flask import (Blueprint, jsonify, request)
import costumer_profile_repository

costumer_profile = Blueprint('costumer_profile', __name__, url_prefix='/costumer_profile')

@costumer_profile.route('/', methods=['POST'])
def generate_customer_profile():
    data = request.get_json()
    result = costumer_profile_repository.generate_customer_profile(data.get('norek'), data.get('start_date'), data.get('end_date'))
    return jsonify(result)

@costumer_profile.route('/', methods=['GET'])
def get_costumer_profile():
    result = costumer_profile_repository.get_costumer_profile()
    return jsonify(result)

@costumer_profile.route('/<int:id>', methods=['GET'])
def get_costumer_profile_by_id(id):
    result = costumer_profile_repository.get_costumer_profile_by_id(id)
    return jsonify(result)