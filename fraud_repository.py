import psycopg2
import psycopg2.extras
from config import config

def getAllFraud():
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cur.execute('SELECT * from fraud_report')
    data = cur.fetchall()
    conn.close()
    return data

def createFraud():
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    query = 'INSERT INTO fraud_report'   
    query += '(norek, jenis_kartu, tipe_channel, waktu_kejadian, nominal, nama_petugas)'  
    query += ' VALUES (%s, %s, %s, %s, %s,%s)'
    cur.execute(query, (data.get("noRekening"), data.get("jenisKartu"), data.get("tipeChannel"), data.get("waktuKejadian"), data.get("nominal"), data.get("petugas")))
    conn.commit()
    conn.close()
    return None