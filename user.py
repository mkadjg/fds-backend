from flask import (Blueprint, jsonify, request)
from werkzeug.security import generate_password_hash
import user_repository

user = Blueprint('user', __name__, url_prefix='/user')

@user.route('/', methods=['GET'])
def get_users():
    result = user_repository.get_users()
    return jsonify(result)

@user.route('/<int:id>', methods=['GET'])
def get_users_by_id(id):
    result = user_repository.get_user_by_id(id)
    return jsonify(result)
    
@user.route('/', methods=['POST'])
def create_user():
    data = request.get_json()
    data['password'] = generate_password_hash(data.get('password'))
    result = user_repository.create_user(data)
    return result

@user.route('/<int:id>', methods=['PUT'])
def update_user(id):
    data = request.get_json()
    result = user_repository.update_user(data, id)
    return result

@user.route('/<int:id>', methods=['DELETE'])
def delete_user(id):
    result = user_repository.delete_user(id)
    return result

