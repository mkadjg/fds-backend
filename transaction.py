from flask import (Blueprint, jsonify, request)
import transaction_repository

transaction = Blueprint('transaction', __name__, url_prefix='/transaction')

@transaction.route('/getTotalTransactionByTipeChanelPerToday', methods=['GET'])
def getTotalTransactionByTipeChanelPerToday():
    allTransaction = transaction_repository.getTotalTransactionByTipeChanelPerToday()
    labels = []
    data = []
    for transaction in allTransaction :
        labels.append(transaction.get('tipe_channel'))
        data.append(transaction.get('frequency'))
    result = {}
    result["labels"] = labels
    result["data"] = data
    return jsonify(result)

@transaction.route('/getCountTransactionByTypeCard', methods=['GET'])
def getCountTransactionByTypeCard():
    allTransaction = transaction_repository.getCountTransactionByTypeCard()
    labels = []
    data = []
    for transaction in allTransaction : 
        labels.append(transaction.get("jenis_kartu"))
        data.append(transaction.get("frequency"))
    
    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@transaction.route('/getTotalTransactionAllStatusToday', methods=['GET'])
def getTotalTransactionAllStatusToday():
    allTransaction = transaction_repository.getTotalTransactionAllStatusToday()
    labels = []
    data = []
    for transaction in allTransaction : 
        labels.append('Berhasil' if transaction.get("status_code") == '00' else 'Gagal')
        data.append(transaction.get("totalfrequency"))
    
    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@transaction.route('/getTotalAmountAllStatusToday', methods=['GET'])
def getTotalAmountAllStatusToday():
    allTransaction = transaction_repository.getTotalAmountAllStatusToday()
    labels = []
    data = []
    for transaction in allTransaction : 
        labels.append('Berhasil' if transaction.get("status_code") == '00' else 'Gagal')
        data.append(transaction.get("totalamount"))
    
    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@transaction.route('/getTotalTransactionLastHour', methods=['GET'])
def getTotalTransactionLastHour():
    allTransaction = transaction_repository.getTotalTransactionLastHour()
    labels = []
    data = []
    for transaction in allTransaction : 
        labels.append(transaction.get("time"))
        data.append(transaction.get("totalfrequency"))
    
    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@transaction.route('/getLast20Transaction', methods=['GET'])
def getLast20Transaction():
    result = transaction_repository.getLast20Transaction()
    return jsonify(result)