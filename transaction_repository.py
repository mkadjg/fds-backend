import psycopg2
import psycopg2.extras
from config import config

def getTotalTransactionByTipeChanelPerToday():
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cur.execute("SELECT tipe_channel, count(*) as frequency FROM public.transactions where EXTRACT(day from timestamp) = EXTRACT(day from now()) and status_code = '00' group by tipe_channel order by tipe_channel")
    data = cur.fetchall()
    conn.close()
    return data

def getCountTransactionByTypeCard():
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cur.execute("select jenis_kartu, count(*) as frequency from transactions group by jenis_kartu")
    data = cur.fetchall()
    conn.close()
    return data

def getTotalTransactionAllStatusToday():
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cur.execute("select status_code, count(*) as totalFrequency from public.transactions where EXTRACT(day from timestamp) = EXTRACT(day from now()) group by status_code")
    data = cur.fetchall()
    conn.close()
    return data

def getTotalAmountAllStatusToday():
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cur.execute("select status_code, sum(trx_amount) as totalAmount from public.transactions where EXTRACT(day from timestamp) = EXTRACT(day from now()) group by status_code")
    data = cur.fetchall()
    conn.close()
    return data

def getTotalTransactionLastHour():
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cur.execute("SELECT to_char(timestamp,'HH:MI') as time, count(*) as totalFrequency FROM public.transactions where timestamp between (now() - interval '60 minute') and now() group by time order by time asc")
    data = cur.fetchall()
    conn.close()
    return data

def getLast20Transaction():
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cur.execute("select * from transactions order by timestamp desc limit 20")
    data = cur.fetchall()
    conn.close()
    return data