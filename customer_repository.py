from database import db

def top5CustomerInternetBankingByFrequency():
    cur = db.get_cursor()
    cur.execute("select norek, count(*) as frequency from transactions where tipe_channel = 'INTERNET BANKING' group by norek order by frequency desc limit 5")
    data = cur.fetchall()
    return data

def top5CustomerEdcByFrequency():
    cur = db.get_cursor()
    cur.execute("select norek, count(*) as frequency from transactions where tipe_channel = 'EDC' group by norek order by frequency desc limit 5")
    data = cur.fetchall()
    return data

def top5CustomerAtmByFrequency():
    cur = db.get_cursor()
    cur.execute("select norek, count(*) as frequency from transactions where tipe_channel = 'ATM' group by norek order by frequency desc limit 5")
    data = cur.fetchall()
    return data

def top5CustomerSmsBankingByFrequency():
    cur = db.get_cursor()
    cur.execute("select norek, count(*) as frequency from transactions where tipe_channel = 'SMS BANKING' group by norek order by frequency desc limit 5")
    data = cur.fetchall()
    return data

def top5CustomerAllByFrequency():
    cur = db.get_cursor()
    cur.execute("select norek, count(*) as frequency from transactions group by norek order by frequency desc limit 5")
    data = cur.fetchall()
    return data

def top5CustomerInternetBankingByAmount():
    cur = db.get_cursor()
    cur.execute("select norek, sum(trx_amount) as total from transactions where tipe_channel = 'INTERNET BANKING' group by norek order by total desc limit 5")
    data = cur.fetchall()
    return data

def top5CustomerEdcByAmount():
    cur = db.get_cursor()
    cur.execute("select norek, sum(trx_amount) as total from transactions where tipe_channel = 'EDC' group by norek order by total desc limit 5")
    data = cur.fetchall()
    return data

def top5CustomerAtmByAmount():
    cur = db.get_cursor()
    cur.execute("select norek, sum(trx_amount) as total from transactions where tipe_channel = 'ATM' group by norek order by total desc limit 5")
    data = cur.fetchall()
    return data

def top5CustomerSmsBankingByAmount():
    cur = db.get_cursor()
    cur.execute("select norek, sum(trx_amount) as total from transactions where tipe_channel = 'SMS BANKING' group by norek order by total desc limit 5")
    data = cur.fetchall()
    return data

def top5CustomerAllByAmount():
    cur = db.get_cursor()
    cur.execute("select norek, sum(trx_amount) as total from transactions group by norek order by total desc limit 5")
    data = cur.fetchall()
    return data