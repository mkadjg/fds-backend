from database import db

def get_roles():
    cur = db.get_cursor()
    cur.execute("select * from roles")
    data = cur.fetchall()
    return data

def get_role_by_id(id):
    cur = db.get_cursor()
    cur.execute("select * from roles where id = %s", (id,))
    data = cur.fetchone()
    return data

def create_role(data):
    cur = db.get_cursor()
    query = "insert into roles (role_name, description) values(%s, %s)"
    cur.execute(query, (data.get('role_name'), data.get('description')))
    return "Success"

def update_role(data, id):
    cur = db.get_cursor()
    cur.execute("update roles set role_name = %s, description = %s where id = %s", (data.get('role_name'), data.get('description'), id))
    return "Success"

def delete_role(id):
    cur = db.get_cursor()
    cur.execute("delete from roles where id = %s", (id,))
    return "Success"