from flask import Flask
from database import db
from fraud import fraud
from transaction import transaction
from dashboard import dashboard
from user import user
from roles import roles
from customer import customer
from costumer_profile import costumer_profile
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

db.init_app(app)
app.register_blueprint(customer)
app.register_blueprint(roles)
app.register_blueprint(user)
app.register_blueprint(transaction)
app.register_blueprint(fraud)
app.register_blueprint(dashboard)
app.register_blueprint(costumer_profile)

if __name__ == '__main__':
	app.run(debug=True)
