from flask import (Blueprint, jsonify, request)
import customer_repository

customer = Blueprint('customer', __name__, url_prefix='/customer')

@customer.route('/top5CustomerInternetBankingByFrequency', methods=['GET'])
def top5CustomerInternetBankingByFrequency():
    allData = customer_repository.top5CustomerInternetBankingByFrequency()
    labels = []
    data = []
    for newData in allData :
        labels.append(newData.get('norek'))
        data.append(newData.get('frequency'))

    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@customer.route('/top5CustomerEdcByFrequency', methods=['GET'])
def top5CustomerEdcByFrequency():
    allData = customer_repository.top5CustomerEdcByFrequency()
    labels = []
    data = []
    for newData in allData :
        labels.append(newData.get('norek'))
        data.append(newData.get('frequency'))

    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@customer.route('/top5CustomerAtmByFrequency', methods=['GET'])
def top5CustomerAtmByFrequency():
    allData = customer_repository.top5CustomerAtmByFrequency()
    labels = []
    data = []
    for newData in allData :
        labels.append(newData.get('norek'))
        data.append(newData.get('frequency'))

    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@customer.route('/top5CustomerSmsBankingByFrequency', methods=['GET'])
def top5CustomerSmsBankingByFrequency():
    allData = customer_repository.top5CustomerSmsBankingByFrequency()
    labels = []
    data = []
    for newData in allData :
        labels.append(newData.get('norek'))
        data.append(newData.get('frequency'))

    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@customer.route('/top5CustomerAllByFrequency', methods=['GET'])
def top5CustomerAllByFrequency():
    allData = customer_repository.top5CustomerAllByFrequency()
    labels = []
    data = []
    for newData in allData :
        labels.append(newData.get('norek'))
        data.append(newData.get('frequency'))

    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)


@customer.route('/top5CustomerInternetBankingByAmount', methods=['GET'])
def top5CustomerInternetBankingByAmount():
    allData = customer_repository.top5CustomerInternetBankingByAmount()
    labels = []
    data = []
    for newData in allData :
        labels.append(newData.get('norek'))
        data.append(newData.get('total'))

    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@customer.route('/top5CustomerEdcByAmount', methods=['GET'])
def top5CustomerEdcByAmount():
    allData = customer_repository.top5CustomerEdcByAmount()
    labels = []
    data = []
    for newData in allData :
        labels.append(newData.get('norek'))
        data.append(newData.get('total'))

    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@customer.route('/top5CustomerAtmByAmount', methods=['GET'])
def top5CustomerAtmByAmount():
    allData = customer_repository.top5CustomerAtmByAmount()
    labels = []
    data = []
    for newData in allData :
        labels.append(newData.get('norek'))
        data.append(newData.get('total'))

    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@customer.route('/top5CustomerSmsBankingByAmount', methods=['GET'])
def top5CustomerSmsBankingByAmount():
    allData = customer_repository.top5CustomerSmsBankingByAmount()
    labels = []
    data = []
    for newData in allData :
        labels.append(newData.get('norek'))
        data.append(newData.get('total'))

    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)

@customer.route('/top5CustomerAllByAmount', methods=['GET'])
def top5CustomerAllByAmount():
    allData = customer_repository.top5CustomerAllByAmount()
    labels = []
    data = []
    for newData in allData :
        labels.append(newData.get('norek'))
        data.append(newData.get('total'))

    result = {
        "labels": labels,
        "data": data
    }
    return jsonify(result)