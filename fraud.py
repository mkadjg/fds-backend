from flask import (Blueprint, jsonify, request)
import fraud_repository

fraud = Blueprint('fraud', __name__, url_prefix='/fraud')

@fraud.route('s', methods=['GET'])
def getAllFraud():
    result = fraud_repository.getAllFraud()
    return jsonify(result)

@fraud.route('', methods=['POST'])
def createFraud():
    data = request.get_json()
    result = fraud_repository.createFraud(data)
    return result

