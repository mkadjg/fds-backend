from flask import g
import psycopg2
import psycopg2.extras
from config import config

class DatabaseConnection(object):
    def init_app(self, app):

        @app.before_request
        def connect():
            params = config()
            g.db = psycopg2.connect(**params)

        @app.teardown_appcontext
        def tear_down_connection(exception):
            if not exception:
                g.db.commit()
            g.db.close()

    def get_cursor(self):
        return g.db.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    
    def get_connection(self):
        return g.db

db = DatabaseConnection()