from flask import (Blueprint, jsonify, request)
import roles_repository
roles = Blueprint('roles', __name__, url_prefix='/roles')

@roles.route('/', methods=['GET'])
def get_roles():
    result = roles_repository.get_roles()
    return jsonify(result)

@roles.route('/<int:id>', methods=['GET'])
def get_role_by_id(id):
    result = roles_repository.get_role_by_id(id)
    return jsonify(result)

@roles.route('/', methods=['POST'])
def create_role():
    data = request.get_json()
    result = roles_repository.create_role(data)
    return result

@roles.route('/<int:id>', methods=['PUT'])
def update_role(id):
    data = request.get_json()
    result = roles_repository.update_role(data, id)
    return result

@roles.route('/<int:id>', methods=['DELETE'])
def delete_role(id):
    result = roles_repository.delete_role(id)
    return result