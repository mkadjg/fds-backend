from database import db
import json

def query1(norek, startDate, endDate):
    cur = db.get_cursor()
    query = "SELECT * FROM public.transactions where norek = %s and timestamp between %s and %s limit 20"
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data

def query2(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''select kategori, count(*) as frequency from (
            select *, case when trx_amount <= 100000 then '< 100rb'
            when trx_amount <= 500000 then '< 500rb'
            when trx_amount <= 1000000 then '< 1jt'
            when trx_amount <= 2000000 then '< 2jt'
            else '> 2jt' end as kategori
            from transactions
            where norek = %s
            and timestamp between %s and %s)
            as result group by kategori order by kategori'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data

def query3(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''SELECT CASE WHEN extract(hour from timestamp) BETWEEN 2 AND 5 THEN '02:00 - 05.59'
            WHEN extract(hour from timestamp) BETWEEN 6 AND 9 THEN '06:00 - 09.59'
            WHEN extract(hour from timestamp) BETWEEN 10 AND 13 THEN '10:00 - 13.59'
            WHEN extract(hour from timestamp) BETWEEN 14 AND 17 THEN '14:00 - 17.59'
            WHEN extract(hour from timestamp) BETWEEN 18 AND 21 THEN '18:00 - 21.59'
            ELSE '22:00 - 01.59'
            END AS time,
            count(*) frequency
            FROM transactions
            WHERE norek = %s
            AND date(timestamp) BETWEEN %s AND %s
            GROUP BY time'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data

def query4(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''select tipe_channel, (extract(hour from timestamp)*60*60 + extract(minute from timestamp)*60 + extract(second from timestamp)) as time, trx_amount 
            from transactions
            where norek = %s
            and tunai = true
            and timestamp between %s and %s
            group by tipe_channel, time, trx_amount
            order by tipe_channel'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data 

def query5(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''select tipe_channel, (extract(hour from timestamp)*60*60 + extract(minute from timestamp)*60 + extract(second from timestamp)) as time, trx_amount 
            from transactions
            where norek = %s
            and tunai = false
            and timestamp between %s and %s
            group by tipe_channel, time, trx_amount
            order by tipe_channel'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data 

def query6(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''select jenis_trx, sum(trx_amount) as total
            from transactions
            where norek = %s
            and tunai = true
            and timestamp between %s and %s
            group by jenis_trx
            order by total desc'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data

def query7(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''select jenis_trx, count(*) as frequency
            from transactions
            where norek = %s
            and tunai = true
            and timestamp between %s and %s
            group by jenis_trx
            order by frequency desc'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data

def query8(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''select jenis_trx, sum(trx_amount) as total
            from transactions
            where norek = %s
            and tunai = false
            and timestamp between %s and %s
            group by jenis_trx
            order by total desc'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data   

def query9(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''select jenis_trx, count(*) as frequency
            from transactions
            where norek = %s
            and tunai = false
            and timestamp between %s and %s
            group by jenis_trx
            order by frequency desc'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data 

def query10(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''select tipe_channel, sum(trx_amount) as total
            from transactions 
            where norek = %s
            and timestamp between %s and %s
            group by tipe_channel 
            order by total desc'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data 

def query11(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''select tipe_channel, count(*) as frequency
            from transactions 
            where norek = %s
            and timestamp between %s and %s
            group by tipe_channel 
            order by frequency desc'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data 

def query12(norek, startDate, endDate):
    cur = db.get_cursor()
    query = '''select tipe_channel, max(trx_amount), min(trx_amount), percentile_disc(0.5) 
            within group (order by trx_amount)
            from transactions 
            where norek = %s
            and timestamp between %s and %s
            group by tipe_channel'''
    cur.execute(query, (norek, startDate, endDate))
    data = cur.fetchall()
    return data

def get_costumer_profile():
    cur = db.get_cursor()
    cur.execute("select id, norek, request, start_date, end_date, status from customer_profile where user_id = 3")
    data = cur.fetchall()
    return data

def get_costumer_profile_by_id(id):
    cur = db.get_cursor()
    query = "select * from customer_profile where id = %s"
    cur.execute(query, (id,))
    data = cur.fetchone()
    return data

def generate_customer_profile(norek, startDate, endDate):
    cur = db.get_cursor()
    cur.execute("insert into customer_profile (user_id, norek, start_date, end_date, status) values(3, %s, %s, %s, 'on progress') returning id", (norek, startDate, endDate))
    id = cur.fetchone()['id']

    result = {}
    result['query1'] = query1(norek, startDate, endDate)
    result['query2'] = query2(norek, startDate, endDate)
    result['query3'] = query3(norek, startDate, endDate)
    result['query4'] = query4(norek, startDate, endDate)
    result['query5'] = query5(norek, startDate, endDate)
    result['query6'] = query6(norek, startDate, endDate)
    result['query7'] = query7(norek, startDate, endDate)
    result['query8'] = query8(norek, startDate, endDate)
    result['query9'] = query9(norek, startDate, endDate)
    result['query10'] = query10(norek, startDate, endDate)
    result['query11'] = query11(norek, startDate, endDate)
    result['query12'] = query12(norek, startDate, endDate)

    cur.execute("update customer_profile set status = 'done', result = %s where id = %s", (json.dumps(result, default=str), id))
    return result