from database import db
import json

def get_users():
    cur = db.get_cursor()
    cur.execute("select a.*, b.role_name from users a inner join roles b on a.role_id = b.id limit 20")
    data = cur.fetchall()
    return data

def get_user_by_id(id):
    cur = db.get_cursor()
    cur.execute("select * from users where id = %s", (id,))
    data = cur.fetchone()
    return data

def create_user(data):
    cur = db.get_cursor()
    query = "insert into users (username, name, role, role_id, password) values (%s, %s, %s, %s, %s)"
    cur.execute(query, (data.get('username'), data.get('name'), data.get('role'), data.get('role_id'), data.get('password')))
    return "Success"

def update_user(data, id):
    cur = db.get_cursor()
    query = "update users set username = %s, name = %s, role = %s, role_id = %s where id = %s"
    cur.execute(query, (data.get('username'), data.get('name'), data.get('role'), data.get('role_id'), id))
    return "Success"

def delete_user(id):
    cur = db.get_cursor()
    cur.execute("delete from users where id = %s", (id,))
    return "Success"