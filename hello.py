from flask import Flask
from flask import request
from flask import jsonify
import psycopg2
import psycopg2.extras
from config import config
from flask_cors import CORS
app = Flask(__name__)
CORS(app)

@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/test', methods=['GET'])
def test():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute('SELECT * from fraud_report')
        rows = cur.fetchall()
        cur.close()
        return jsonify(rows)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getCountTransactionByTypeChannel', methods=['GET'])
def getCountTransactionByTypeChannel():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select tipe_channel, count(*) as countFailed from transactions where status_code = \'51\' group by tipe_channel")
        failedTransaction = cur.fetchall()
        cur.execute("select tipe_channel, count(*) as countSucces from transactions where status_code = \'00\' group by tipe_channel")
        sucsessTransaction = cur.fetchall()
        result = []
        result.append({
            "label" : "sukses",
            "data" : sucsessTransaction
        })
        result.append({
            "label" : "gagal",
            "data" : failedTransaction
        })
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getCountTransactionByTypeCard', methods=['GET'])
def getCountTransactionByTypeCard():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select jenis_kartu, count(*) as frequency from transactions group by jenis_kartu")
        allTransaction = cur.fetchall()
        labels = []
        data = []
        for x in allTransaction : 
            labels.append(x.get("jenis_kartu"))
            data.append(x.get("frequency"))
        
        result = {
            "labels": labels,
            "data": data
        }
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTop3FailedChannel', methods=['GET'])
def getTop3FailedChannel():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select tipe_channel, count(*) as countFailed from transactions where status_code = \'51\' group by tipe_channel order by countFailed desc limit 3")
        result = cur.fetchall()
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTop5FailedHour', methods=['GET'])
def getTop5FailedHour():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("SELECT EXTRACT(hour from timestamp) as time, count(*) as countFailed FROM transactions where status_code = \'51\' group by time order by gagal desc limit 5")
        result = cur.fetchall()
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTopFailedAccount', methods=['GET'])
def getTopFailedAccount():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("SELECT norek, count(*) as countFailed FROM transactions where status_code = \'51\' group by norek order by countFailed desc limit 1")
        result = cur.fetchall()
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getCostum', methods=['GET'])
def getCostum():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select * from public.transactions where status_code = \'00\' and norek = (SELECT norek FROM public.transactions where status_code = \'51\' group by norek order by count(*) desc limit 1) and EXTRACT(hour from timestamp) = (SELECT EXTRACT(hour from timestamp) as time FROM public.transactions where status_code = \'51\' group by time order by count(*) desc limit 1)")
        result = cur.fetchall()
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTransactionBetweenLastMont', methods=['GET'])
def getTransactionBetweenLastMont():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select count(*) as frequencyTransaction from public.transactions where timestamp between (now() - interval \'30 day\') and now()")
        result = cur.fetchall()
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTransactionBetween10DayPerStatus', methods=['GET'])
def getTransactionBetween10DayPerCard():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select count(*) as frequencyTransaction from public.transactions where timestamp between (now() - interval \'30 day\') and now() and status_code = \'51\'")
        failedTransaction = cur.fetchall()
        cur.execute("select count(*) as frequencyTransaction from public.transactions where timestamp between (now() - interval \'30 day\') and now() and status_code = \'00\'")
        successTransaction = cur.fetchall()
        result = []
        result.append({
            "label": "success",
            "frequencyTransaction" : successTransaction[0].get('frequencytransaction')
        })
        result.append({
            "label": "failed",
            "frequencyTransaction" : failedTransaction[0].get('frequencytransaction')
        })
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTotalAmount10DayLater', methods=['GET'])
def getTotalAmount10DayLater():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select sum(trx_amount) as amount from public.transactions where timestamp between (now() - interval \'30 day\') and now() and status_code = \'51\'")
        failedTransaction = cur.fetchall()
        cur.execute("select sum(trx_amount) as amount from public.transactions where timestamp between (now() - interval \'30 day\') and now() and status_code = \'00\'")
        successTransaction = cur.fetchall()
        result = []
        result.append({
            "label": "success",
            "total_amount" : successTransaction[0].get('amount')
        })
        result.append({
            "label": "failed",
            "total_amount" : failedTransaction[0].get('amount')
        })
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getLast20Transaction', methods=['GET'])
def getLast20Transaction():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select * from transactions order by timestamp desc limit 20")
        result = cur.fetchall()
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getAllFraudReport', methods=['GET'])
def getAllFraudReport():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select * from fraud_report")
        result = cur.fetchall()
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTotalFraudReportPerCard', methods=['GET'])
def getTotalFraudReportPerCard():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select jenis_kartu, count(*) as totalFraudReport from fraud_report group by jenis_kartu")
        result = cur.fetchall()
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTotalNominePerChannel', methods=['GET'])
def getTotalNominePerChannel():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select tipe_channel, sum(nominal) as totalNominal from fraud_report group by tipe_channel")
        result = cur.fetchall()
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTotalTransactionLastHour', methods=['GET'])
def getTotalTransactionLastMinute():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("SELECT to_char(timestamp,'HH:MI') as time, count(*) as totalFrequency FROM public.transactions where timestamp between (now() - interval '60 minute') and now() group by time order by time asc")
        allTransaction = cur.fetchall()
        labels = []
        data = []
        for x in allTransaction : 
            labels.append(x.get("time"))
            data.append(x.get("totalfrequency"))
        
        result = {
            "labels": labels,
            "data": data
        }
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTotalTransactionToday', methods=['GET'])
def getTotalTransactionToday():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("SELECT count(*) as totalFrequency FROM public.transactions where EXTRACT(day from timestamp) = EXTRACT(day from now())")
        result = cur.fetchall()
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTotalTransactionAllStatusToday', methods=['GET'])
def getTotalTransactionAllStatusToday():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select status_code, count(*) as totalFrequency from public.transactions where EXTRACT(day from timestamp) = EXTRACT(day from now()) group by status_code")
        allTransaction = cur.fetchall()
        labels = []
        data = []
        for x in allTransaction : 
            labels.append('Berhasil' if x.get("status_code") == '00' else 'Gagal')
            data.append(x.get("totalfrequency"))
        
        result = {
            "labels": labels,
            "data": data
        }
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


@app.route('/getTotalAmountAllStatusToday', methods=['GET'])
def getTotalAmountAllStatusToday():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("select status_code, sum(trx_amount) as totalAmount from public.transactions where EXTRACT(day from timestamp) = EXTRACT(day from now()) group by status_code")
        allTransaction = cur.fetchall()
        labels = []
        data = []
        for x in allTransaction : 
            labels.append('Berhasil' if x.get("status_code") == '00' else 'Gagal')
            data.append(x.get("totalamount"))
        
        result = {
            "labels": labels,
            "data": data
        }
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/getTotalTransactionByTipeChanelPerToday', methods=['GET'])
def getTotalTransactionByTipeChanelPerToday():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute("SELECT tipe_channel, count(*) as frequency FROM public.transactions where EXTRACT(day from timestamp) = EXTRACT(day from now()) and status_code = '00' group by tipe_channel order by tipe_channel")
        temp = cur.fetchall()
        labels = []
        data = []
        for x in temp :
            labels.append(x.get('tipe_channel'))
            data.append(x.get('frequency'))
        result = {}
        result["labels"] = labels
        result["data"] = data
        return jsonify(result)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

@app.route('/insertDataFraudReport', methods=['POST'])
def insertDataFraudReport():
    conn = None
    try:
        data = request.get_json()
        params = config()
        conn = psycopg2.connect(**params)
        query = 'INSERT INTO fraud_report'   
        query += '(norek, jenis_kartu, tipe_channel, waktu_kejadian, nominal, nama_petugas)'  
        query += ' VALUES (%s, %s, %s, %s, %s,%s)'
        cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        cur.execute(query, (data.get("noRekening"), data.get("jenisKartu"), data.get("tipeChannel"), data.get("waktuKejadian"), data.get("nominal"), data.get("petugas")))
        conn.commit()
        return "success"
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')